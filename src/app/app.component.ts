import { Component, OnInit } from '@angular/core';
import { LibraryService } from './service/library.service';
import { Library } from './library';
import { OrderBy } from './orderPipe';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'CRUDAngular';
  library: Array<Library>;
  fav: Array<Library>;
  orderLibrary;

  constructor(private libraryService: LibraryService, private orderby: OrderBy) { }

  ngOnInit() {
    this.libraryService.getLibrary().subscribe(res => {
      this.library = res;
    });

    this.libraryService.getFav().subscribe(res => {
      this.fav = res;
    });
  }

  orderBy(value) {
    this.library = this.orderby.transform(this.library, value);
  }

  addFav(value) {
    this.library[value].favourite = !this.library[value].favourite;
    if (this.library[value].favourite == true)
      this.libraryService.postFav(this.library[value]).subscribe(res => {
        this.fav = res;
      });
    else {
      this.libraryService.deleteFav(this.library[value]).subscribe(res => {
        this.fav = res;
      });
    }
  }


}