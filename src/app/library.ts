export class Library {
    title: string;
    author: string;
    genre: string;
    pageNbr: number;
    favourite: boolean;
}
