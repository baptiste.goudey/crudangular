import { Injectable } from '@angular/core';
import { Library } from '../library';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LibraryService {
  library: Array<Library> = [
    { title: 'Along Came a Spider', author: 'JAMES PATTERSON', genre: 'PD', pageNbr: 528, favourite: false },
    { title: 'Kiss the Girls', author: 'JAMES PATTERSON', genre: 'PD', pageNbr: 496, favourite: false },
    { title: 'Jack & Jill', author: 'JAMES PATTERSON', genre: 'PD', pageNbr: 480, favourite: false },
    { title: 'Double Crossed', author: 'ALLY CARTER', genre: 'AA', pageNbr: 89, favourite: false },
    { title: 'UNITED WE SPY', author: 'ALLY CARTER', genre: 'E', pageNbr: 101, favourite: true },
  ];
  favs = [];

  constructor() { }

  getLibrary() {
    return of(this.library);
  }

  getFav() {
    for (let i = 0; this.library[i] != null; i++) {
      if (this.library[i].favourite == true) {
        this.favs.push(this.library[i]);
      }
    }
    return of(this.favs);
  }

  postFav(book) {
    this.favs.push(book);
    return of(this.favs);
  }

  deleteFav(book) {
    let favTmp = [];
    let bookindex = this.findBooks(book);

    for (let i = 0; this.favs[i] != null; i++) {
      if(i != bookindex)
        favTmp.push(this.favs[i]);
    }
    this.favs = favTmp;
    return of(this.favs);
  }

  findBooks(book) {
    for (let i = 0; this.favs[i] != null; i++) {
      if (this.favs[i].title == book.title) {
        return i;
      }
    }
    return -1;
  }
}
